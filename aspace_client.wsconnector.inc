<?php

if (class_exists('restclient_wsconnector')) {

  class aspace_client_wsconnector extends restclient_wsconnector {
      
    /**
     * This is a slightly modified version of the wscall method defined in the restclient
     * modules's aspace_client_wsconnector class. The main difference is an authentication
     * step added just prior to the REST method switch(). Each time we use a REST method
     * to access an ArchivesSpace server, we need to include an X-ArchiveSpace-Session header,
     * containing a valid authorization token (supplied by the AS server). if we don't have this
     * auth token (or if the one we've stored has expired), then we need to generate a new one.
     *
     * Implements WsConnector->wscall().
     */
    public function wscall($type, $method, $arguments, $options) {
      $this->restclientPrepareWsCall($type, $method, $arguments, $options);

      // check speficied error handling behaviour from options
      $errorHandling = array_key_exists('error_handling', $options);
      
      //
      // Authentication
      //
      // return $options array, populated with the ArchiveSpace header (name/value pair)
      // return FALSE if we can't authenticate to the ArchivesSpace server.
      //
      $options = $this->authenticate($type, $method, $arguments, $options);
      
      // proceed with REST call, if header is set properly for AS server access
      
      if (is_array($options) && $this->aspace_header_validate($options)) {
        switch ($type) {
          case 'create':
            return $this->restclientCreate($type, $method, $arguments, $options, $errorHandling);
          case 'read':
            return $this->restclientRead($type, $method, $arguments, $options, $errorHandling);
          case 'update':
            return $this->restclientUpdate($type, $method, $arguments, $options, $errorHandling);
          case 'delete':
            return $this->restclientDelete($type, $method, $arguments, $options, $errorHandling);
          case 'index':
            return $this->restclientIndex($type, $method, $arguments, $options, $errorHandling);
  	      default:
            // Make an actionable request (i.e. generic POST)
            if (drupal_substr($type,0,7) == 'action_') {
              $variables = array('endpoint' => $this->endpoint, 'body' => $arguments) + $options;
              if (empty($arguments)) {
                unset($variables['body']);
              }
              $response = restclient_post($method, $variables);
              if (restclient_response_code($response) != RESTCLIENT_RESPONSE_SUCCESS) {
                if ($errorHandling) {
                  // don't cache error responses
                  $this->expires = 0;
                  return $response;
                }
                else {
                  return FALSE;
                }
              }
              else {
                return $response->data;
              }
           }
        }
      }
      else {
        return FALSE;
      }
    }
    
    /*
     * Authenticate with the ArchivesSpace server (if necessary);
     * Return the REST method's $options array with the X-ArchiveSpace-Session header added in
     */
    public function authenticate($type, $method, $arguments, $options) {
      $authenticated = FALSE;
      $auth_token = '';
      
      // retrieve the stored auth token (if we have one), then validate it, determining whether
      // we need to authenticate with the ArchivesSpace server (and request/receive a new session token from it).     
   
      $auth_token = $this->auth_token_get();
      
      if ($this->auth_token_validate($auth_token)) {
        $authenticated = TRUE;
      }
      else {
        // we need to get a new auth_token from the AS server.
        $auth_token = $this->auth_token_generate();
        
        if ($auth_token) {
          $authenticated = TRUE;
        }
      }
      
      // if authentication was successful, then return the $options
      // array with the ArchivesSpace session header added in
      
      if ($authenticated) {
        $options['header']['X-ArchivesSpace-Session'] = $auth_token;
      }
      else {
        $options = FALSE;
        //unset($options['header']['X-ArchivesSpace-Session']);
      }
      return $options;
    }
    
    /*
     * Confirm that the AS header is set correctly
     */
    public function aspace_header_validate($options) {
      $result = FALSE;
      
      if (isset($options['header']['X-ArchivesSpace-Session']) &&
          !empty($options['header']['X-ArchivesSpace-Session'])) {
        $result = TRUE;
      }
      return $result;
    }
     
    /*
     * Confirm that we have a valid authorization token
     */
    public function auth_token_validate($auth_token) {
      $result = (!empty($auth_token)) ? TRUE : FALSE;
      return $result;
    }
    
    /*
     * Generate a new authorization token
     *
     * Once a valid authorization token is received from the AS server,
     * it is cached for future REST calls. Also note that although the
     * login credentials for the AS server are stored in settings.php
     * (and therefore appear in the global $conf), they do not appear in
     * the cache_bootstrap table along with other variables instantiated
     * by variable_set().
     */
    public function auth_token_generate() {
      global $conf;        
      $auth_token = FALSE;
      
      // make sure that login credentials for AS server are stored in settings.php;
      // if not, then fail immediately.
      
      if (!isset($conf['aspace_client_username']) || 
          !isset($conf['aspace_client_password']) ||
          empty($conf['aspace_client_username']) ||
          empty($conf['aspace_client_password'])) {
        return $auth_token;
      }
      
      // send authentication request to the AS server, using POST method; the server
      // response should contain a freshly-generated authorization token.
      
      $response = restclient_post('/users/%username/login?password=%password', $variables = array(
        'parameters' => array(
          '%username' => $conf['aspace_client_username'],
          '%password' => $conf['aspace_client_password'],
        ),
      ));
      
      // process the response from the AS server
      
      if (restclient_response_code($response) != RESTCLIENT_RESPONSE_SUCCESS) {
        //dpm('Authentication failed.');
      }
      else {
        if (isset($response->data)) {
          $auth_data = drupal_json_decode($response->data);
          
          if (isset($auth_data['session'])) {
            $auth_token = $auth_data['session'];
          }
        }
      }
      
      // make sure we received a valid auth_token from the AS server
      
      if ($this->auth_token_validate($auth_token)) {
        $auth_token = $this->auth_token_set($auth_token);
      }
      else {
        //dpm("Failed! Couldn't generate a fresh auth token.");
      }
      return $auth_token;
    }
    
    // @todo
    // might want to store auth_token in some other place? since
    // variables are (apparently) only loaded at bootstrap.
    // maybe add our own db table for this purpose. or use
    // a static var?
    
    /*
     * Load the stored authorization token
     */
    public function auth_token_get() {
      $auth_token = variable_get('aspace_client_auth_token');
      return $auth_token;
    }
    
    /*
     * Store the authorization token
     */
    public function auth_token_set($auth_token) {
      $stored_auth_token = TRUE;
      variable_set('aspace_client_auth_token', $auth_token);
      return $stored_auth_token;
    }
  } // end of class
}